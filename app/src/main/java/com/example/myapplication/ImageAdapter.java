package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageViewHolder> {


    private Context mContext;
    private List<Upload> mUploads;

    public ImageAdapter(Context context , List<Upload> uploads){

        mContext = context;
        mUploads = uploads;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(mContext).inflate(R.layout.images_set,parent,false);
        return new ImageViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {

        Upload upload  = mUploads.get(position);
        holder.mTextView.setText(upload.getName());
        Picasso.with(mContext)
                .load(upload.getUrl())
                .fit()
                .centerCrop()
                .into(holder.mImageView);
    }

    @Override
    public int getItemCount() {
        return mUploads.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder{


        public TextView mTextView;
        public ImageView mImageView;


        public ImageViewHolder(@NonNull View itemView) {


            super(itemView);

            mTextView = itemView.findViewById(R.id.nameText);
            mImageView = itemView.findViewById(R.id.image_view);

        }
    }
}
