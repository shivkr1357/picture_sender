package com.example.myapplication;

public class Upload {

    private String mImangeName;
    private String mImageUrl;

    public Upload()
    {
        //cannot invoke parameterized constructor without default constructor
    }

    public Upload(String imageNmae , String imageUrl)
    {

        if(imageNmae.trim().equals("")) {
            imageNmae = "no name";
        }

        mImangeName = imageNmae;
        mImageUrl = imageUrl;

    }

    public String getName()
    {
        return mImangeName;
    }
    public void setName(String name)
    {
        mImangeName = name;
    }
    public String getUrl()
    {
        return mImageUrl;
    }
    public void setUrl(String Url)
    {
        mImageUrl = Url;
    }
}
